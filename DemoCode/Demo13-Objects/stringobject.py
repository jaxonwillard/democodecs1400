amount = 2
numberOfOneDollars = 3
numberOfDimes = 4
numberOfNickels = 5
numberOfPennies = 6
numberOfQuarters = 1




print("Your amount", amount, "consists of\n",
"\t", numberOfOneDollars, "dollars\n",
"\t", numberOfQuarters, "quarters\n",
"\t", numberOfDimes,  "dimes\n",
"\t", numberOfNickels, "nickels\n",
"\t", numberOfPennies, "pennies")