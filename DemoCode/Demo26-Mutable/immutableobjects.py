def main():
    # Numbers are immutable objects
    life1 = 100
    life2 = 500

    print("Life 1:,", life1)
    use_life(life1)
    print("Life 1:,", life1)

    print("Life 2:", life2)
    use_life(life2)
    print("Life 2:", life2)


def use_life(life):
    life = life * 0.8

main()