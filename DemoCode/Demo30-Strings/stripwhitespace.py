str_1 = "    lots of words    "
print(str_1 + "Next String")
print(str_1.strip() + "Next String")
print(str_1.lstrip() + "Next String")
print(str_1.rstrip() + "Next String")

print()
str_1 = "\t\tlots of words\t\t"
print(str_1 + "Next String")
print(str_1.strip() + "Next String")
print(str_1.lstrip() + "Next String")
print(str_1.rstrip() + "Next String")