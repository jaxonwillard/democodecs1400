# Change this string to test things out
str_1 = "This123is 123 a string123"

print("Check String Properties:", str_1)
print("isalnum():", str_1.isalnum())
print("isalpha():", str_1.isalpha())
print("isnumeric():", str_1.isnumeric())
print("isdigit():", str_1.isdigit())
print("isidentifier():", str_1.isidentifier())
print("islower():", str_1.islower())
print("isupper():", str_1.isupper())
print("isspace():", str_1.isspace())

print()
print("Search String Content:", str_1)
print("endswith():", str_1.endswith("123"))
print("startswith():", str_1.startswith("123"))
print("find():", str_1.find("123"))
print("rfind():", str_1.rfind("123"))
print("count():", str_1.count("123"))

print()
print("Convert Strings:", str_1)
print("capitalize():", str_1.capitalize())
print("lower():", str_1.lower())
print("upper():", str_1.upper())
print("title():", str_1.title())
print("swapcase():", str_1.swapcase())
print("replace():", str_1.replace("a", "X"))

