def main():
    print("Concatenation (+): ", end="")
    print("First String" + "Second String")
    print()

    print("Repetition (*): ", end="")
    print("One String" * 6)
    print()

    print("Index ([index]): ", end="")
    str_1 = "This is a string"
    print(str_1[12])
    print()

    print("Slice ([start:end]): ", end="")
    str_2 = "Slice this string"
    print(str_2[3:12])
    print()

    print("in: ", end="")
    str_3 = "Look in this string"
    print("ook" in str_3, end=" ")
    print("zzz" in str_3)
    print()


main()