# is is the same as comparing id's

str_1 = "Hello"
str_2 = str_1
str_3 = "Goodbye"
str_4 = "Hello" * 6
str_5 = "Hello" * 6

print(str_1 is str_2, id(str_1) == id(str_2))
print(str_1 is str_3, id(str_1) == id(str_3))
print(str_4 is str_5, id(str_4) == id(str_5))

