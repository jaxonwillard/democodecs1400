import random


def fav_num():
    num = random.randint(0, 5)
    if num != 2:
        return num
    else:
        return

def main():
    res = fav_num()
    if res == None:
        print("You don't have a favorite number")
    else:
        print("Your favorite number is", res)


main()
