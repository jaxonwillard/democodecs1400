def with_return():
    print("I will return a value")
    return 10


def no_return():
    print("I do not return anything")


def main():
    print(with_return())
    print(no_return())


main()
