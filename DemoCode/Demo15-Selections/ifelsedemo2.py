import random

num1 = random.randint(0, 99)
num2 = random.randint(0, 99)

user_answer = input("What is " + str(num1) + " + " + str(num2) + "? ")

if user_answer.isdigit():
    user_answer = eval(user_answer)
    actual_answer = num1 + num2

    if user_answer == actual_answer:
        message = "correct!"
    else:
        message = "incorrect."

    print("Your answer is", message)
else:
    print("Your input is not valid")