import math

class Circle:
    def __init__(self, radius=1):
        self.radius = radius

    def get_perimeter(self):
        return 2 * math.pi * self.radius

    def get_area(self):
        return math.pi * self.radius ** 2

    def set_radius(self, radius):
        self.radius = radius