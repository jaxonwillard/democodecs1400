from circle import Circle

def main():
    cir1 = Circle()
    cir2 = Circle(5)

    print("Circle 1", cir1.get_perimeter())
    print("Circle 2", cir2.get_perimeter())

    cir1.set_radius(20)
    print("Circle 1", cir1.get_perimeter())


main()