import random


def main():
    list_1 = []
    for i in range(40):
        list_1.append(random.randint(30,100))
    key = 85
    result = linear_search(list_1, key)
    if result != -1:
        print("Found,", key, "at position", result)
    else:
        print("Did not find", key)


def linear_search(input_list, key):
    for i in range(len(input_list)):
        if key == input_list[i]:
            return i
    return -1


main()