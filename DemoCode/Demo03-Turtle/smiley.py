"""
I didn't put comments in here so you can read/modify the code to figure out
what everything means
"""

import turtle

turtle.showturtle()
turtle.penup()
turtle.goto(-200, -100)
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("yellow")
turtle.circle(100)
turtle.end_fill()
turtle.penup()
turtle.goto(-240, 0)
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("black")

turtle.circle(20)
turtle.penup()
turtle.goto(-160, 0)
turtle.pendown()
turtle.circle(20)
turtle.end_fill()

turtle.penup()
turtle.goto(-260, -40)
turtle.setheading(315)
turtle.pendown()
turtle.width(10)
turtle.circle(90, 90)
turtle.hideturtle()
turtle.done()