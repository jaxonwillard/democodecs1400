import math


class Circle:
    def __init__(self, radius):
        self.radius = radius # This is an instance variable
        other_radius = radius # This is a local variable

    def get_perimeter_instance_variable(self):
        # Instance variables are in scope throughout the class
        return 2 * math.pi * self.radius

    def get_perimeter_local_variable(self):
        # Local variables are only in scope in the function
        # So this wouldn't work
        return 2 * math.pi * other_radius


