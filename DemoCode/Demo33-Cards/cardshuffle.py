import random

def main():
    deck = list(range(52))
    suits = ["Spades", "Clubs", "Hearts", "Diamonds"]
    ranks = ["Ace", "Two", "Three", "Four", "Five", "Six",
             "Seven", "Eight", "Nine", "Ten", "Jack", "Queen",
             "King"]

    random.shuffle(deck)

    while len(deck) > 0:
        card = deck.pop()
        print(ranks[card % 13] + " of " + suits[card // 13])


main()