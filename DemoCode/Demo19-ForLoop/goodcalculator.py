# Here is a calculator that is a little more robust
# It still has issues though! What are they?
print("Welcome to the Amazing Multiplication Table Calculator")

beg_num = eval(input("Enter the beginning number: "))
end_num = eval(input("Enter the ending number: "))
step = eval(input("Enter the step size: "))

num_space = len(str(end_num * end_num)) + 2
row_header_space = len(str(end_num)) + 2
total_width = row_header_space + num_space * ((end_num - beg_num) // step + 1)

print(format("Multiplication Table", "^" + str(total_width) + "s"))
print()
print(format("", str(row_header_space)), end="")

for i in range(beg_num, end_num + 1, step):
    print(format(i, ">" + str(num_space) + "d"), end="")
print()

for i in range(0, total_width):
    print("-", end="")
print()

for i in range(beg_num, end_num + 1, step):
    print(format(str(i) + " |", ">" + str(row_header_space) + "s"), end="")
    for j in range(beg_num, end_num + 1, step):
        print(format(i * j, ">" + str(num_space) + "d"), end="")
    print()




